FROM alpine:3.18
RUN apk add --no-cache nginx
COPY ./html /usr/share/nginx/html

EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]